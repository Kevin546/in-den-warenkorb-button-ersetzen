import CartButton from './cart_button/cart_button.plugin';
import CheckoutCartButton from './cart_button/checkout_cart_button.plugin';
import EmzOffCanvasCart from './emz-offcanvas-cart/emz-offcanvas-cart.plugin';

const PluginManager = window.PluginManager;
PluginManager.register('CartButton', CartButton, '[cart-button-plugin]');
PluginManager.register('CheckoutCartButton', CheckoutCartButton, '[checkout-cart-button-plugin]');
PluginManager.override('OffCanvasCart', EmzOffCanvasCart, '[data-offcanvas-cart]');


if (module.hot) {
    module.hot.accept();
}


