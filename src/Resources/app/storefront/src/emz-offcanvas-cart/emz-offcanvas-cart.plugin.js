import OffCanvasCartPlugin from 'src/plugin/offcanvas-cart/offcanvas-cart.plugin';

export default class EmzOffCanvasCart extends OffCanvasCartPlugin {

    init() {
        super.init();
    }
 
    _registerEmzQuantityInputFieldEvents() {

        this.offcanvasminPurchase = [];
        this.offcanvasmaxPurchase = [];
        this.offcanvaspurchaseSteps = [];
        this.offcanvaspurchaseRange = [];

        this.changeEvent = new Event('change');  
        this.offcanvasinputFieldItemQuantity = document.querySelectorAll('.offcanvas-item-quantity-input');
        this.offcanvasplusButton = document.querySelectorAll('.offcanvas-plus-quantity-input');      
        this.offcanvasminusButton = document.querySelectorAll('.offcanvas-minus-quantity-input');

        for(let i = 0; i < this.offcanvasinputFieldItemQuantity.length; i++)
        {  
            this.offcanvasinputFieldItemQuantity[i].addEventListener('blur', (e) => {
                if (e.handled !== true) 
                {
                    e.handled = true;
                    this.offcanvasControlPriceRangeOnBlur(i);      
                    return;
                }
            });

            this.offcanvasplusButton[i].addEventListener('click', (e) => {
                if (e.handled !== true) 
                {
                    e.handled = true;
                    this.offcanvasAddItemQuantity(i);      
                    return;
                }
            });

            this.offcanvasminusButton[i].addEventListener('click', (e) => {
                if (e.handled !== true) 
                {
                    e.handled = true;
                    this.offcanvasDecreaseItemQuantity(i);      
                    return;
                }
            });
            
            this.offcanvasminPurchase[i] = parseInt(this.offcanvasinputFieldItemQuantity[i].dataset.minPurchase);
            this.offcanvasmaxPurchase[i] = parseInt(this.offcanvasinputFieldItemQuantity[i].dataset.maxPurchase);
            this.offcanvaspurchaseSteps[i] = parseInt(this.offcanvasinputFieldItemQuantity[i].dataset.purchaseSteps);
            this.offcanvaspurchaseRange[i] = this.offcanvasCalculatePurchaseRange(this.offcanvasminPurchase[i], this.offcanvasmaxPurchase[i], this.offcanvaspurchaseSteps[i]);    
        }
    }    

    offcanvasCalculatePurchaseRange(start, stop, step){
        let purchaseRangeHelperArray = [];
        for (let x = start; x < stop; x += step) {
            purchaseRangeHelperArray.push(x);
        }
        return purchaseRangeHelperArray;
    }


    offcanvasControlPriceRangeOnBlur(i){

        this.offcanvascurrentItemQuantity = parseInt(this.offcanvasinputFieldItemQuantity[i].value, 10);
        if(this.offcanvaspurchaseRange[i].indexOf(this.offcanvascurrentItemQuantity) == -1) {
            this.offcanvasinputFieldItemQuantity[i].value = this.offcanvaspurchaseRange[i][0];
        }
    }


    offcanvasAddItemQuantity(i){

        this.offcanvasinputFieldItemQuantity[i].value = parseInt(this.offcanvasinputFieldItemQuantity[i].value, 10) + parseInt(this.offcanvaspurchaseSteps[i]);
        this.offcanvascurrentItemQuantity = parseInt(this.offcanvasinputFieldItemQuantity[i].value, 10);

        if(this.offcanvaspurchaseRange[i].indexOf(this.offcanvascurrentItemQuantity) == -1) {
            this.offcanvasinputFieldItemQuantity[i].value = this.offcanvaspurchaseRange[i][this.offcanvaspurchaseRange[i].length - 1];        
            this.offcanvasinputFieldItemQuantity[i].dispatchEvent(this.changeEvent);          
        }

        this.offcanvasinputFieldItemQuantity[i].dispatchEvent(this.changeEvent);  
    }

    offcanvasDecreaseItemQuantity(i){
        this.offcanvasinputFieldItemQuantity[i].value = parseInt(this.offcanvasinputFieldItemQuantity[i].value, 10) - parseInt(this.offcanvaspurchaseSteps[i]);
        this.offcanvascurrentItemQuantity = parseInt(this.offcanvasinputFieldItemQuantity[i].value, 10);

        if(this.offcanvaspurchaseRange[i].indexOf(this.offcanvascurrentItemQuantity) == -1) {
            this.offcanvasinputFieldItemQuantity[i].value = this.offcanvaspurchaseRange[i][0];
            this.offcanvasinputFieldItemQuantity[i].onchange();
            this.offcanvasinputFieldItemQuantity[i].dispatchEvent(this.changeEvent);    
        }

        this.offcanvasinputFieldItemQuantity[i].dispatchEvent(this.changeEvent);  
    }

    _registerEvents() {
        this._registerRemoveProductTriggerEvents();
        this._registerChangeQuantityProductTriggerEvents();
        this._registeraddPromotionTriggerEvents();
        this._registerEmzQuantityInputFieldEvents();
        

        if (this._isShippingAvailable()) {
            this._registerUpdateShippingEvents();
            this._registerToggleShippingSelection();
        }

        this.$emitter.publish('registerEvents');
    }
     
}

